# Etapa 0, políticas:
- Definir el sistema de tickets internos. Podemos usar los _issues_ de gitlab.
- Definir canal de comunicación con los usuarios.
- Definir canal de comunicación con el vendor.
- Definir el organigrama del centro: Autoridades y responsables.


# Etapa 1, validación:
- Verificar la conectividad. Documentar las redes que hay en el sistema y formas de acceso remoto.
- Que IPs y sistemas están expuestos al exterior.
- Analizar opciones de monitoreo disponible para:
  - Carga de CPU y GPU
  - Consumo electrico
  - Refrigeración de la sala (en principio, bajo impacto, refrigeración por agua)
  - Storage
  - Cómo se reciben alertas

- Inventariar componentes y repuestos.

- Documentar infra y administración del storage.

- Stress test del sistema. Intentar generar carga de trabajo en un nodo y en todo el cluster y monitorear.

## Analizar y documentar tareas recurrentes:
- Sistema de provisioning (y como hacer si queremos agregar algo en la instalación de los nodos).
- Administración remota.
- Prender y apagar nodos.
- Simular corte eléctrico.
- Documentar proceso de encendido y apagado controlado.
- Reinicio de GPU (por driver? reiniciar el nodo? prolog de slurm?).
- Sistema de creacion de usuarios y accounting. Slurmdb, quotas de horas.
- Prueba de corte por falla en refrigeración.

# Etapa2, puesta en marcha:
- Abrir el uso inicial a usuarios avanzados.
- Armar un stack de software con spack que incluya como externals el software instalado por el vendor.
- Armar un entorno python que aproveche las GPUs.
- Evaluar performance: rendimiento de las GPUs, uso correcto de HBM.
- Investigar si funciona la herramienta para portar de CUDA a SYCL.
- Documentar el uso de cada aplicación.
- Nodo de visualizacion: Analizar software disponible y documentar un caso de uso.


# Etapa 3, mantenimiento:
- Monitorear.
- Abrir cuentas.
- Soporte a usuarios.
- Actualización y mejora en el stack de aplicaciones.
- Atender fallas de hardware / infra.
- Parches de seguridad y comunicación con Lenovo.
- Evaluar nuevas tecnologías o funcionalidades.
- Optimizar el sistema en base al uso que se le está dando.
- Aplicar los cambios necesarios para imlementar nuevas políticas que pudieran surgir.
- Actualizar la wiki para usuarios con las consultas mas frecuentes.
